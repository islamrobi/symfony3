<?php
namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;

class AboutController extends Controller
{
    /**
     * @Route("/about")
     */
    public function indexAction(){
        $number = mt_rand(0, 100);

        return $this->render('about/home.html.twig', array(
            'number' => $number
        ));
    }
}